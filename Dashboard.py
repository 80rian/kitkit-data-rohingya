from pyathena import connect
import mysql.connector
import pandas as pd
import os

class Dashboard:

    def __init__(self):
        AWS = {'AWS_ACCESS_KEY_ID': os.environ['AWS_ACCESS_KEY_ID'],
                'AWS_SECRET_ACCESS_KEY': os.environ['AWS_SECRET_ACCESS_KEY'],
                'S3_STAGING_DIR': os.environ['S3_STAGING_DIR'],
                'REGION_NAME': os.environ['REGION_NAME']}
        
        self.athena = connect(aws_access_key_id=AWS['AWS_ACCESS_KEY_ID'],
                                aws_secret_access_key=AWS['AWS_SECRET_ACCESS_KEY'],
                                s3_staging_dir=AWS['S3_STAGING_DIR'],
                                region_name=AWS['REGION_NAME'])
        
        self.conn = mysql.connector.connect(host='inhouse-env.ckvv5limtfqs.us-west-2.rds.amazonaws.com',
                                       database='dashboard',
                                       user='admin',
                                       password='3ReIy79PSyJRXVm6wywd')
        
        self.cursor = self.conn.cursor()
    

    def etl(self):
        query = """SELECT device, subject, egg, gamename, level, COUNT(*) AS try
                    FROM dw_src.tb_src_rohingya_xprize
                    WHERE gamename IN ('EggQuizMath', 'EggQuizLiteracy', 'EggQuiz_MiddleTest')
                    AND act = 'start'
                    GROUP BY 1, 2, 3, 4, 5
                    ORDER BY 1, 2, 3, 4, 5;"""
        
        df = pd.read_sql(query, self.athena)
        print(f"data extracted. data length: {len(df)}")

        for index, row in df.iterrows():
            update_query = f"""INSERT IGNORE INTO tbl_kitkit_eggquiz (device, subject, egg, gamename, level, try)
                                VALUES('{row['device']}', '{row['subject']}', '{row['egg']}', '{row['gamename']}', '{row['level']}', '{row['try']}')"""
            self.cursor.execute(update_query)
            self.conn.commit()
            text = f"{index+1}/{len(df)}"
            print(text)
        
        self.cursor.close()
        self.conn.close()

if __name__=='__main__':
    t = Dashboard()
    t.etl()