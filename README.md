# KITKIT-IRC Rohingya

### 전처리

**1차 전처리**
```bash
python3 raw_to_src.py
```

raw_to_src.py는 kitkit-archive 버킷에 있는 raw 파일들을 인스펙션하고 1차 가공한 뒤 data-analysis-adrian 버킷으로 옮기는 작업이다.

파일 인스펙션은 아래와 같다.
- 로그 파일이다.
- 파일 용량이 0보다 크다.
- gzip인 경우 압축 해제에 에러가 발생하지 않는다.
- txt 파일의 각 라인이 적합한 json 형태이다.
- 최근 일주일 내에 업로드 되었다.


**2차 전처리**
```bash
python3 src_to_fact.py
```

src_to_fact.py는 1차 전처리된 파일들을 분석 가능한 형태로 가공하고 athena의 dw_src.tb_src_rohingya에 적재한다.

분석에 용이한 테이블을 만들기 위해서 xprize, kitkittest, todoschoollibrary 패키지를 하나의 테이블로 묶었으며 그 외의 패키지는 가공 및 적재에서 제외했다.

dw_src.tb_src_rohingya는 camp와 package로 partition 되어있다.



### 분석
작업중