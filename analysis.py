import pandas as pd
import numpy as np
from pyathena import connect
import os
from datetime import datetime
from slack_report import slack_report
import gspread
from gspread_dataframe import set_with_dataframe
from oauth2client.service_account import ServiceAccountCredentials


# AWS = {'AWS_ACCESS_KEY_ID': os.environ['AWS_ACCESS_KEY_ID'],
#        'AWS_SECRET_ACCESS_KEY': os.environ['AWS_SECRET_ACCESS_KEY'],
#        'S3_STAGING_DIR': os.environ['S3_STAGING_DIR'],
#        'REGION_NAME': os.environ['REGION_NAME']}

# AWS = {'AWS_ACCESS_KEY_ID': 'AKIASCHPJFR7KPZO2KMU',
#        'AWS_SECRET_ACCESS_KEY': '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',
#        'S3_STAGING_DIR': 's3://enuma-dw',
#        'REGION_NAME': 'us-west-2'}

# athena = connect(aws_access_key_id=AWS['AWS_ACCESS_KEY_ID'],
#                 aws_secret_access_key=AWS['AWS_SECRET_ACCESS_KEY'],
#                 s3_staging_dir=AWS['S3_STAGING_DIR'],
#                 region_name=AWS['REGION_NAME'])

# query = 'SELECT device, user, local FROM dw_src.tb_src_rohingya_xprize ORDER BY device, local'
# base_df = pd.read_sql(query, athena)

base_df = pd.read_csv('/home/ubuntu/script/R/rohingya.csv')
#base_df = pd.read_csv('rohingya.csv')
base_df['local'] = base_df['local'].astype('datetime64[ns]')
base_df['date'] = base_df['local'].dt.date
base_df['unique_session'] = 1
base_df = base_df.loc[base_df.local >= '2019-01-01']
base_df = base_df.sort_values(by=['device', 'local']).reset_index(drop=True)
session_key = 1


for index, row in base_df.iterrows():
    
    try:
        if base_df.iloc[index, 0] == base_df.iloc[index-1, 0]:
            tdelta = base_df.iloc[index, 5] - base_df.iloc[index-1, 5]
            if tdelta.seconds >= 600:
                session_key += 1
                print(f'{index} added')
        elif base_df.iloc[index]['device'] != base_df.iloc[index-1]['device']:
            session_key = 1
            print(f'{index} reset')
        
        base_df.iloc[index, 22] = session_key

        print(index)
    except:
        print(f'{index} failed')
        slack_report('@adrian', f'{index} failed')
        pass

base_df.to_csv('rohingya.csv')
slack_report('@adrian', 'csv saved')

base_df_summary = base_df.groupby('date').agg({'unique_session':pd.Series.nunique})

scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

credentials = ServiceAccountCredentials.from_json_keyfile_name('adrian-2fb24000dfc9.json', scope)

gc = gspread.authorize(credentials)

wks = gc.open_by_key('1Im_OsfMfWBLV2upXJhl-QqJkBTw9BnCjGTbi_jbf6xA')
attendance_sheet = wks.get_worksheet(0)
set_with_dataframe(attendance_sheet, base_df_summary)

slack_report('@adrian', 'done')