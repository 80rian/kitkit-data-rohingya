import boto3
import pandas as pd
import json
from io import StringIO, BytesIO
import zipfile
from datetime import datetime, timedelta
import re
from slack_report import slack_report

def raw_to_src(prefix):

    start_time = datetime.now()
    date_stand = datetime.now() - timedelta(days=7)
    s3 = boto3.client('s3')
    key_list = []
    bucket = 'kitkit-archive'
    kwargs = {'Bucket':bucket, 'Prefix':prefix, 'MaxKeys':1000}

    while True:
        response = s3.list_objects_v2(**kwargs)
        for element in response['Contents']:
            if element['LastModified'].isoformat() >= date_stand.isoformat():
                key = element['Key']
                ext = key.split('.')[-1]
                if (ext == 'txt' and 'crash' not in key) or ext == 'zip':
                    key_list.append(key)
            else:
                pass
            
        if response['IsTruncated']:
            kwargs['ContinuationToken'] = response['NextContinuationToken']
        else:
            break
    
    work_count = 0
    for index, key in enumerate(key_list):

        device = key.split('/')[1].split('_')[0]

        if device.isdigit() == False:
            continue

        filename = key.split('/')[-1].replace('zip', 'csv').replace('txt', 'csv')
        ext = key.split('.')[-1]
        destination = f'kitkit/src/{prefix}/{device}/{filename}'
        df_list = []
        df = pd.DataFrame()
        
        obj = s3.get_object(Bucket=bucket, Key=key)
        if obj['ContentLength'] == 0:
            print(f'[{index}/{len(key_list)} passed] {key}')
            continue
        
        try:
            if ext == 'txt':
                obj_df = pd.read_csv(obj['Body'], delimiter='\n', header=None, names=['json'])
                #pd.read_json might work if and only if all lines are valid json

                for row in obj_df.iterrows():
                    try:
                        string = json.loads(row[1][0])
                        row_df = pd.io.json.json_normalize(string)
                        df_list.append(row_df)
                    except:
                        pass
            
            elif ext == 'zip':
                z = zipfile.ZipFile(BytesIO(obj['Body'].read()))
                for line in z.open(z.namelist()[0]).readlines():
                    try:
                        string = json.loads(line.decode('utf-8'))
                        row_df = pd.io.json.json_normalize(string)
                        df_list.append(row_df)
                    except:
                        pass
            
            df = pd.concat(df_list, sort=True)
            df.reset_index(inplace=True, drop=True)
            df['device'] = device

            csv_buffer = StringIO()
            df.to_csv(csv_buffer, index=False)
            s3.delete_object(Bucket='data-analysis-adrian', Key=destination)
            s3.put_object(Bucket='data-analysis-adrian', Key=destination, Body=csv_buffer.getvalue())
        except:
            pass

        work_count += 1
        print(f'[{index}/{len(key_list)} completed] {key}')
        
    
    end_time = datetime.now()
    duration = str(end_time - start_time)
    print(duration)

    slack_report(channel='#bi_report_log', text=f'{prefix} raw_to_src completed. file count:{work_count} / time:{duration}')



if __name__=='__main__':
    raw_to_src(prefix='rohingya-irc-2019-10')

