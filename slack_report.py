import slack
import datetime
import os
import time

def slack_report(channel, text):
    SLACK_BOT_TOKEN = 'xoxb-7177913874-668390328867-YJbaGYA2BlswOIcDbVkOnmIF'
    client = slack.WebClient(token=SLACK_BOT_TOKEN)
    os.environ['TZ'] = 'UTC'
    time.tzset()
    send_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    client.chat_postMessage(
        channel=channel,
        text=f"```[{send_time}] {text}```",
        as_user=True)
