import boto3
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from io import StringIO, BytesIO
from slack_report import slack_report
import re

def src_to_fact(prefix):

    start_time = datetime.now()
    date_stand = datetime.now() - timedelta(days=7)
    s3 = boto3.client('s3')
    athena = boto3.client('athena', region_name='us-west-2')
    key_list = []
    bucket = 'data-analysis-adrian'
    prefix = f'kitkit/src/{prefix}'
    kwargs = {'Bucket':bucket, 'Prefix':prefix, 'MaxKeys':1000}
    device = pd.read_csv('device.txt', delimiter='\n', header=None, names=['device'])
    file_type_dict = {
        'com_enuma_xprize':'xprize', 
        'library_todoschool_enuma_com_todoschoollibrary':'library_video',
        'com_enuma_kitkittest':'kitkittest',
        'com_enuma_booktest':'library_book'
        }
    col_list = [
        'device', 
        'user',
        'id',
        'session_id',
        'sntp',
        'local',
        'act',
        'content',
        'duration',
        'scene',
        'subject',
        'egg',
        'day',
        'contentname',
        'level',
        'answer_level',
        'answer_progress',
        'answer_worksheet',
        'problem',
        'subtask',
        'subtaskcount',
        'timercount',
        'isdummy',
        'expected',
        'response',
        'result']

    while True:
        response = s3.list_objects_v2(**kwargs)
        for element in response['Contents']:
            if element['LastModified'].isoformat() >= date_stand.isoformat():
                key = element['Key']
                if any(keyword in key for keyword in file_type_dict.keys()):
                    key_list.append(key)
            else:
                continue

        if response['IsTruncated']:
            kwargs['ContinuationToken'] = response['NextContinuationToken']
        else:
            break
    
    work_count = 0
    for index, key in enumerate(key_list):

        filename = key.split('/')[-1].replace('zip', 'csv').replace('txt', 'csv')
        camp = '-'.join(prefix.split('-')[2:])
        package = key.split('/')[-1].split('.')[0]
        file_type = file_type_dict[package]
        
        
        obj = s3.get_object(Bucket=bucket, Key=key)
        obj_df = pd.read_csv(obj['Body'])
        obj_df.columns = obj_df.columns.map(lambda x: x.split(".")[-1].lower())

        if obj['ContentLength'] == 0:
            continue


        if file_type == 'xprize':
            
            obj_df = obj_df.loc[~((obj_df.scene=='freeChoice') & (((obj_df.act=='open') | (obj_df.act=='close'))))]

            if len(obj_df) == 0:
                continue
            elif 'problem' not in obj_df.columns:
                continue

            obj_df[['answer_level', 'answer_progress', 'answer_worksheet']] = obj_df.problem.str.split('/', expand=True)
            obj_df['correct'] = None

            if 'response' not in obj_df.columns:
                obj_df['response'] = None
            
            if 'duration' not in obj_df.columns:
                obj_df['duration'] = None

            if 'expected' in obj_df.columns:
                obj_df.loc[~obj_df.answer_level.isnull(), 'result'] = 'Wrong'
                obj_df.loc[(~obj_df.answer_level.isnull()) & (obj_df.expected == obj_df.response), 'result'] = 'Correct'
            else:
                obj_df['expected'] = None
                obj_df['result'] = None

            obj_df['subject'] = obj_df['where'].str.replace('en-US_', '').str.split('_', expand=True)[0]
            obj_df['level'] = None
            obj_df.loc[obj_df.content=='game', 'level'] = obj_df['where'].str.split('\/', expand=True)[3]
            obj_df['egg'] = obj_df['where'].str.split('/', expand=True)[0].str.split('_', expand=True)[2]
            obj_df['day'] = np.nan
            try:
                obj_df.loc[obj_df.scene=='daily', 'day'] = obj_df.loc[obj_df.scene=='daily', 'where'].str.split('/', expand=True)[1]
            except:
                pass
            obj_df['contentname'] = None
            obj_df.loc[(obj_df.scene=='game') | (obj_df.scene=='daily'), 'contentname'] = obj_df.loc[(obj_df.scene=='game') | (obj_df.scene=='daily'), 'where'].str.split('/', expand=True)[2]
            try:
                obj_df.loc[obj_df.scene=='freeChoice', 'contentname'] = obj_df.loc[obj_df.scene=='freeChoice', 'where'].str.split('/', expand=True)[1]
            except:
                obj_df.loc[obj_df.scene=='freeChoice', 'contentname'] = None

        
        elif file_type == 'library_video':
            obj_df['content'] = obj_df['scene'].map(lambda x: x.split('_')[-1])
            obj_df['response'] = obj_df['response']/1000
            obj_df = obj_df.rename(columns={'where':'contentname', 'response':'duration'})
        
        elif file_type == 'library_book':
            obj_df['content'] = obj_df['scene'].map(lambda x: x.split('_')[-1])
            obj_df['response'] = ''
            obj_df = obj_df.rename(columns={'where':'contentname', 'response':'duration'})


        elif file_type == 'kitkittest':
            obj_df = obj_df.rename(columns={'answer':'expected', 'choice':'response'})
        
        else:
            continue


        for col in col_list:
            if col not in obj_df.columns:
                obj_df[col] = None
        
        df = obj_df[col_list]
        df = df[df.device.isin(device.device)]
        if len(df) == 0:
            continue


        destination = f'kitkit/fact/rohingya-irc/camp={camp}/package={file_type}/{filename}'   

        csv_buffer = StringIO()
        df = df.replace("", np.nan)
        df.to_csv(csv_buffer, index=False)
        s3.delete_object(Bucket='data-analysis-adrian', Key=destination)
        s3.put_object(Bucket='data-analysis-adrian', Key=destination, Body=csv_buffer.getvalue())

        work_count += 1
        print(f'[{index}/{len(key_list)} completed] {key}')

    end_time = datetime.now()
    duration = str(end_time - start_time)
    print(duration)

    res = athena.start_query_execution(
    QueryString="MSCK REPAIR TABLE dw_src.tb_src_rohingya;",
    QueryExecutionContext={
        'Database': 'dw-src'
    },
    ResultConfiguration={
        'OutputLocation': 's3://aws-athena-query-results-142235741310-us-west-2/Unsaved'
    })

    slack_report(channel='#bi_report_log', text=f'{prefix} src_to_fact completed. file count:{work_count} / time:{duration}')

if __name__=='__main__':
    src_to_fact(prefix='rohingya-irc-2019-10')



"""
CREATE EXTERNAL TABLE dw_src.tb_src_rohingya (
device string,
`user` string,
id int,
session_id int,
sntp string,
`local` timestamp,
act string,
content string,
duration string,
scene string,
subject string,
egg string,
day string,
contentname string,
level string,
answer_level string,
answer_progress string,
answer_worksheet string,
problem string,
subtask string,
subtaskcount string,
timercount string,
isdummy boolean,
expected string,
response string,
result string)
PARTITIONED BY ( 
  `camp` string,
  `package` string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ("separatorChar" = ",", "escapeChar" = "\\") 
LOCATION 's3://data-analysis-adrian/kitkit/fact/rohingya-irc/'
TBLPROPERTIES ("skip.header.line.count"="1")
"""